import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  serverElements = [{type:"server",name:"Server 1",content:"This is Server one"}];

  onServerAdded(serverData:{serverName:string,serverContent:string}) {
    this.serverElements.push({
      type: 'server',
      name: serverData.serverName,
      content: serverData.serverContent
    });
  }

  onAddBlueprintAdded(serverData:{serverName:string,serverContent:string}) {
    this.serverElements.push({
      type: 'blueprint',
      name: serverData.serverName,
      content: serverData.serverContent
    });
  }
  onChangeFirst(){
    this.serverElements[0].name="Changed";
  }
  //called when something gets removed from the existing DOM.
  onDestroyFirst(){
    this.serverElements.splice(0,1);
  }
}
