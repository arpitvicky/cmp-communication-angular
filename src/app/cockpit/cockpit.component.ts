import { Component, OnInit, EventEmitter, Output,ViewEncapsulation,ViewChild,ElementRef } from '@angular/core';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.css']
  // encapsulation:ViewEncapsulation.None
})
export class CockpitComponent implements OnInit {
  // newServerName = '';
  newServerContent = '';
  @ViewChild('serverContentInput') serverContentInput : ElementRef;
  // creating eventemitter objecta which allows us to emit our own event and then sending out through Output decorator.
  @Output() serverCreated = new EventEmitter<{serverName:string,serverContent:string}>();
  @Output() bluePrintAdded = new EventEmitter<{serverName:string,serverContent:string}>();

//creating alias of the event emitted.from the calling component it will have to bind to bpcreated.
//  @Output('bpCreated') bluePrintAdded = new EventEmitter<{serverName:string,serverContent:string}>();

  constructor() { }

  ngOnInit() {
  }
  

  onAddServer(serverNameInput:HTMLInputElement) {
    console.log(this.serverContentInput.nativeElement.value); //@ViewChild approach to fetch value from html field (local reference).
    console.log(serverNameInput); //only local reference approach and the parameter received as function parameters.
    this.serverCreated.emit({
      serverName:serverNameInput.value,
      // serverContent:this.newServerContent
      serverContent:this.serverContentInput.nativeElement.value
    });
  }

  onAddBlueprint(serverNameInput:HTMLInputElement) {
    this.bluePrintAdded.emit({
      serverName:serverNameInput.value,
      // serverContent:this.newServerContent
      serverContent:this.serverContentInput.nativeElement.value
    });
  }

}
