import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-app-game',
  templateUrl: './app-game.component.html',
  styleUrls: ['./app-game.component.css']
})
export class AppGameComponent implements OnInit {
  counterValue:number;
  // oddCheck:boolean=false;
  // evenCheck:boolean=false;
  oddArray:number[]=[];
  evenArray:number[]=[];
  constructor() { }

  ngOnInit() {
  }
  OnCounterIncrementFired(counter:number){
    this.counterValue = counter;
    
    if(counter%2 === 0){
      this.evenArray.push(counter);
    }
    else{
      this.oddArray.push(counter);
    }

    // if(counter%2 === 0){
    //   this.evenCheck=true;
    //   this.oddCheck=false;
    // }
    // else{
    //   this.oddCheck=true;
    //   this.evenCheck=false;
    // }
  }
}
