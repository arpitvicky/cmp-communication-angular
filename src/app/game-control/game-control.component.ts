import { Component, OnInit,EventEmitter,Output} from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {

 timerStart;
 counter:number=0;
//  emitting event from game-control , here this event-emitter is a number value. We make this listenable from outside.
 @Output() counterIncrementFired = new EventEmitter<number>();
  constructor() { }

  ngOnInit() {
  }
  onStart(){
    this.timerStart=setInterval(()=>{
        this.counterIncrementFired.emit(this.counter++);
    },1000);
  }
  onStop(){
    clearInterval(this.timerStart);
  }
}
