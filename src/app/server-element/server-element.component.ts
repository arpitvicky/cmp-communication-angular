import { Component, OnInit, Input,OnChanges ,SimpleChanges,DoCheck,
  AfterContentInit,AfterContentChecked,AfterViewInit,AfterViewChecked,OnDestroy,ViewChild,ElementRef,ContentChild} from '@angular/core';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css']
})
export class ServerElementComponent implements OnInit, OnChanges,DoCheck,AfterContentInit,AfterContentChecked
,AfterViewInit,AfterViewChecked,OnDestroy {
  // Here through Input decorator we are exposing element variable from this component
  //@Input () element:{type:string,name:string,content:string};

  //Adding alias to element property, exposing the element property through different name.Something coming into the component.Bindable from outside
  @Input ('srvElement') element:{type:string,name:string,content:string};
  @Input () name:string;
  @ViewChild('heading') header : ElementRef;
  @ContentChild ('contentParagraph') contentParagraph:ElementRef;
  constructor() {
    console.log('constructor called');
  }

  ngOnInit() {
    console.log('ngOnInit called');
    // console.log('Text content is '+this.header.nativeElement.textContent);
  }

  //usefull when we want to store previous values.
  ngOnChanges(changes:SimpleChanges){
    console.log("Change detected");
    console.log(changes);
  }
  //whenere any changes occurs
  ngDoCheck(){
    console.log("ngDoCheck called");
  }
  // called once for ng-content
  ngAfterContentInit(){
    console.log('ngAfterContentInit called');
    // console.log('Text content is '+this.header.nativeElement.textContent);
   // console.log('The content is' + this.contentParagraph.nativeElement.textContent);
  }
  // called everytime content changes
  ngAfterContentChecked(){
    console.log('ngAfterContentChecked called');
    // console.log('The content is' + this.contentParagraph.nativeElement.textContent);
  }
  ngAfterViewInit(){
    console.log('ngAfterViewInit called');
    // console.log('Text content is '+this.header.nativeElement.textContent);
    console.log('The content is' + this.contentParagraph.nativeElement.textContent);
  }
  ngAfterViewChecked(){
    // console.log('Text content is '+this.header.nativeElement.textContent);
    console.log('ngAfterViewChecked called');
  }
  ngOnDestroy(){
    console.log('ngOnDestroy called');
  }
}
